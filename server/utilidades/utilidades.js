const crearMensaje = (nombre, mensaje) => {
  console.log(nombre);
  return {
    nombre,
    mensaje,
    fecha: new Date().getTime(),
  };
};

module.exports = {
  crearMensaje,
};
